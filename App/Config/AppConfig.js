// Simple React Native specific changes

export default {
  // font scaling override - RN default is on
  activeOpacity: 0.8,
  allowTextFontScaling: true
}
