import { StyleSheet } from 'react-native'
import {Metrics, Colors} from '../../Themes'

export default StyleSheet.create({
  inputComponent: {
    flex: 1,
    marginVertical: Metrics.smallMargin,
    // justifyContent: 'flex-start'
  },
  label: {
    color: Colors.grey
  },
  textInputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    borderColor: Colors.lightGrey,
    borderWidth: 1,
    borderRadius: 10
  },
  leftIcon: {
    marginHorizontal: Metrics.baseMargin
  },
  errorInfoMessage: {
    flexDirection: 'row',
  }
})
