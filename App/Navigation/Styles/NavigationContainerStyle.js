
import { Metrics, Colors, Fonts } from '../../Themes/'
import { Platform } from 'react-native'

const navBarBottomBorder = Platform.OS === 'ios' ? { borderBottomWidth: 1, borderBottomColor: 'transparent' } : {}
export default {
  container: {
    flex: 1
  },
  leftButton: {
    color: Colors.snow,
    tintColor: Colors.snow
  },
  rightButtonContainer: {
    marginHorizontal: Metrics.baseMargin

  },
  appBarTitle: {
    flex: 1,
    fontSize: 16,
    justifyContent: 'center',
    alignSelf: 'center',
    textAlign: 'center',
    fontFamily: Fonts.type.base,
    color: Colors.snow
  },
  navBarTextScreens: {
    fontSize: 16,
    textAlign: 'center',
    alignSelf: 'center',
    fontFamily: Fonts.type.base,
    color: Colors.snow
  },
  subNavText: {
    fontFamily: Fonts.type.normal,
    fontSize: Fonts.size.medium,
    alignSelf: 'center',
    textAlign: 'center',
    marginHorizontal: Metrics.baseMargin,
    backgroundColor: Colors.transparent
  },
  longTitle: {
    alignSelf: 'center',
    color: Colors.silver,
    width: Metrics.screenWidth - 50,
    marginLeft: Metrics.doubleBaseMargin
  },
  navBarStyle: {
    backgroundColor: Colors.themeColor,
    ...navBarBottomBorder
  },
  LeftButton: {
    marginLeft: Metrics.baseMargin
  },
  switchContainer: {
    flexDirection: 'row',
    paddingVertical: Metrics.baseMargin,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.fire,
    borderBottomColor: Colors.silver,
    borderBottomWidth: 1
  },
  tabBar: {
    height: 56,
    borderTopWidth: 1,
    borderTopColor: Colors.themeColor,
    backgroundColor: Colors.background
  },
  tab: {
    flex: 1,
    alignItems: 'stretch'
  },
  overrideText: {},
  title: {
    color: Colors.snow,
    letterSpacing: 1,
    textAlign: 'center',
    fontFamily: Fonts.type.medium,
    backgroundColor: Colors.transparent,
    paddingHorizontal: Metrics.baseMargin
  },
  rightButtonText: {
    textAlign: 'center',
    fontFamily: Fonts.type.medium,
    color: Colors.snow
  },
  backButtonContainer: {
    marginHorizontal: Metrics.baseMargin,
    paddingTop: 1
  }
}
