import React, { Component } from 'react'
import { ScrollView, Text, SafeAreaView, View, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import CardView from 'react-native-cardview'

// Styles
import styles from './styles'
import { Colors } from '../../Themes'
import FullButton from '../../Components/FullButton'

const BUTTONS = [
  { icon: 'camera', text: 'Allow to access camera and photos' },
  { icon: 'camera', text: 'Allow to access apps' },
  { icon: 'camera', text: 'Allow to access browser search' },
  { icon: 'camera', text: 'Allow to access contacts' }
]
export default class LaunchScreen extends Component {
  renderButton = ({icon, text}) => {
    return (
      <CardView
        cardElevation={4}
        cardMaxElevation={4}
        cornerRadius={8}
        style={{
          margin: 10,
          backgroundColor: 'white'
        }}
      >
        <TouchableOpacity key={text} style={[styles.cardButton]}>
          <View style={styles.contentCover}>
            <Icon name={icon} size={24} color={Colors.grey} />
            <Text style={styles.cardText}>{text}</Text>
          </View>
          <Icon name='check-circle' size={22} color={Colors.check} />
        </TouchableOpacity>
      </CardView>
    )
  }
  render () {
    return (
      <SafeAreaView style={styles.mainContainer}>
        <ScrollView
          contentContainerStyle={styles.container}>
          <Text style={styles.titleText}>Permissions</Text>
          <Text style={styles.descriptionText}>
            To use the application features you need to give this application all following permissions.
          </Text>
          <View style={styles.buttonCover}>
            {BUTTONS.map(this.renderButton)}
          </View>

          <View style={styles.bottomView}>
            <View style={[styles.contentCover, { paddingHorizontal: 20 }]}>
              <Icon name='check-circle' size={20} color={Colors.black} />
              <Text style={styles.privacyText}>
                I read the Privacy policy and I accept the terms and conditions.
              </Text>
            </View>
            <FullButton text='Allow' />
          </View>
        </ScrollView>
      </SafeAreaView>
    )
  }
}
