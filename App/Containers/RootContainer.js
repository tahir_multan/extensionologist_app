import React, { Component } from 'react'
import { View, StatusBar } from 'react-native'
import { Actions } from 'react-native-router-flux'

import { connect } from 'react-redux'
import StartupActions from '../Redux/StartupRedux'
import ReduxPersist from '../Config/ReduxPersist'
import ReduxRouter, { ReduxNavigator } from '../Navigation/NavigationRouter'

// Styles
import styles from './Styles/RootContainerStyles'

class RootContainer extends Component {
  componentDidMount () {
    // if redux persist is not active fire startup action
    // if (!ReduxPersist.active) {
    //   this.props.startup()
    // }
  }

  backHandler = () => {
    const prevScene = Actions.currentScene
    Actions.pop()
    return Actions.currentScene !== prevScene
  }

  render () {
    return (
      <View style={styles.applicationView}>
        <StatusBar barStyle='light-content' />
        <ReduxRouter
          navigator={ReduxNavigator}
          backAndroidHandler={this.backHandler} />
      </View>
    )
  }
}

// wraps dispatch to create nicer functions to call within our component
const mapDispatchToProps = (dispatch) => ({
  startup: () => dispatch(StartupActions.startup())
})

export default connect(null, mapDispatchToProps)(RootContainer)
