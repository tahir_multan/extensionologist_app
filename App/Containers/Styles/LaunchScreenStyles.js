import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    padding: Metrics.section
  },
  titleText: {
    fontSize: Fonts.size.h2,
    fontFamily: Fonts.type.bold,
    marginTop: Metrics.section
  },
  descriptionText: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.base,
    marginTop: Metrics.baseMargin
  },
  cardText: {
    flex: 1,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base
  },
  cardButton: {
    flex: 1,
    padding: 15,
    margin: 10,
    elevation: 4,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  }
})
