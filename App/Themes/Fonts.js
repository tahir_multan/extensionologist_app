const type = {
  bold: 'Montserrat-Bold',
  light: 'Montserrat-Light',
  base: 'Montserrat-Regular',
  medium: 'Montserrat-Medium',
  semiBold: 'Montserrat-SemiBold',

  interBold: 'Inter-Bold',
  interSemi: 'Inter-SemiBold',
  interMedium: 'Inter-Medium',
  interLight: 'Inter-LightBETA',
  interRegular: 'Inter-Regular',
  interExBold: 'Inter-ExtraBold'
}

const size = {
  h1: 38,
  h2: 34,
  h3: 32,
  h4: 26,
  h5: 20,
  h6: 19,
  input: 18,
  regular: 17,
  medium: 14,
  standard: 13,
  small: 12,
  tiny: 8.5
}

const style = {
  h1: {
    fontFamily: type.base,
    fontSize: size.h1
  },
  h2: {
    fontWeight: 'bold',
    fontSize: size.h2
  },
  h3: {
    fontFamily: type.emphasis,
    fontSize: size.h3
  },
  h4: {
    fontFamily: type.base,
    fontSize: size.h4
  },
  h5: {
    fontFamily: type.base,
    fontSize: size.h5
  },
  h6: {
    fontFamily: type.emphasis,
    fontSize: size.h6
  },
  normal: {
    fontFamily: type.base,
    fontSize: size.regular
  },
  description: {
    fontFamily: type.base,
    fontSize: size.medium
  }
}

export default {
  type,
  size,
  style
}
