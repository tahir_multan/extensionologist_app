const colors = {
  black: 'black',
  snow: 'white',
  lightGrey: '#E4E4EB',
  grey: '#8F90A6',
  themeColor: '#967325',
  red: '#FF3B3B',
  blue: '#3081F9',
  check: '#3ACC6C',
  borderColor: '#E4E4EB',
  shadowBorder: 'rgb(214, 214, 214)'

}

export default colors
