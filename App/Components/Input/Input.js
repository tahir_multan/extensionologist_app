import React from 'react'
import styles from './styles'
import {View, Text, TextInput} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {Colors} from '../../Themes/'

const Input = (props) => {
  const {
    properties: {
      textInputStyles = {color: Colors.grey, flex: 1},
      onChangeText,
      value,
      label,
      placeholder,
      placeholderTextColor = Colors.grey,
      errorInfoMessage,
      errorInfoMessageIcon,
      errorInfoMessageIconSize = 15,
      errorInfoMessageIconColor,
      leftIcon,
      leftIconSize = 25,
      leftIconColor = Colors.lightGrey,
      onPressLeftIcon = () => {},
      rightIcon,
      rightIconColor = Colors.lightGrey,
      rightIconSize = 25,
      secure = false,
      onPressRightIcon = () => {},
      editable = true
    } = {}
  } = props
  return (
    <View style={[styles.inputComponent, textInputStyles]}>
      {label && <Text style={styles.label}>{label}</Text>}
      <View style={styles.textInputContainer}>
        {leftIcon &&
          <Icon name={leftIcon}
            size={leftIconSize}
            color={leftIconColor}
            style={styles.leftIcon}
            onPress={onPressLeftIcon}
          />
        }
        <TextInput value={value}
          style={textInputStyles}
          onChangeText={(text) => onChangeText(text)}
          editable={editable}
          secureTextEntry={secure}
          placeholder={placeholder}
          placeholderTextColor={placeholderTextColor}
        />
        {rightIcon &&
          <Icon name={rightIcon}
            size={rightIconSize}
            color={rightIconColor}
            style={styles.leftIcon}
            onPress={onPressRightIcon}

          />
        }
      </View>
      {errorInfoMessage &&
      <View style={styles.errorInfoMessage}>
        {errorInfoMessageIcon &&
        <Icon name={errorInfoMessageIcon} size={errorInfoMessageIconSize} color={errorInfoMessageIconColor} />
        }
        <Text style={styles.label}>{errorInfoMessage}</Text>
      </View>
      }
    </View>
  )
}
export default Input
