import React, { useState } from 'react'
import styles from './styles'
import { View, Text, ScrollView, ImageBackground, TouchableOpacity } from 'react-native'
import Input from '../../Components/Input'
import i18n from 'i18n-js'
// eslint-disable-next-line camelcase
import { sign_in_inputs } from '../../Constants/constant'
import {Images} from '../../Themes'

const OnBoarding = () => {
  const [invalidEmail, setInvalidEmail] = useState(false)
  const [invalidPassword, setInvalidPassword] = useState(false)
  const [data, setData] = useState({})
  return (
    <ScrollView contentContainerStyle={styles.main}>
      <ImageBackground source={Images.backgroundImage} style={styles.backgroundImage}>
        <View style={styles.heading}>
          <Text style={styles.headingText}>{i18n.t('signIn.headingSignIn')}</Text>
          <Text style={styles.signUpText}>{i18n.t('signIn.headingSignUp')}</Text>
        </View>
        <View style={styles.body}>
          {sign_in_inputs.map(input => {
            const {key = ''} = input
            return <Input properties={{...input,
              onChangeText: (text) => setData({...data, [data[key]]: text})
            }} />
          })}
          <TouchableOpacity style={styles.button}>
            <Text style={styles.buttonText}>Sign In</Text>
          </TouchableOpacity>
          <Text style={styles.forgotPassword}>Forgot your Password?</Text>
          <TouchableOpacity style={[styles.button, styles.faceRecognition]}>
            <Text style={styles.buttonText}>Sign In with face recognition</Text>
          </TouchableOpacity>
          <Text>OR</Text>
        </View>
      </ImageBackground>
    </ScrollView>
  )
}

export default OnBoarding
