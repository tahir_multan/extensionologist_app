
import React from 'react'
import { connect } from 'react-redux'
import { Actions, Router, Scene } from 'react-native-router-flux'
import { createReactNavigationReduxMiddleware, createReduxContainer } from 'react-navigation-redux-helpers'
import SignIn from '../Containers/SignIn'
import SignUp from '../Containers/SignUp'
import OnBoarding from '../Containers/Onboarding'
import ForgotPassword from '../Containers/ForgotPassword'
import Permissions from '../Containers/Permissions'

import styles from './Styles/NavigationContainerStyle'
import { Colors } from '../Themes'

export const navigationMiddleware = createReactNavigationReduxMiddleware(state => state.nav)
export const Routes = Actions.create(
  <Scene
    headerMode='screen'
    headerTintColor={Colors.snow}
    navigationBarStyle={styles.navBarStyle}
    titleStyle={styles.navBarTextScreens}
    backButtonTintColor={Colors.themeColor}>
    <Scene hideNavBar key='signIn' component={SignIn} />
    <Scene hideNavBar key='signUp' component={SignUp} />
    <Scene hideNavBar key='onBoarding' component={OnBoarding} />
    <Scene hideNavBar key='forgotPassword' component={ForgotPassword} />
    <Scene initial hideNavBar key='permissions' component={Permissions} />
  </Scene>
)

export const ReduxNavigator = createReduxContainer(Routes)
export default connect(state => ({ state: state.nav }))(Router)
