
import { Routes } from '../../App/Navigation/NavigationRouter'

export const reducer = (state, action) => {
  const nextState = Routes.router.getStateForAction(action, state)
  return nextState || state
}
