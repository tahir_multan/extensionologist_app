import {Colors} from '../Themes'
import i18n from 'i18n-js'

// eslint-disable-next-line camelcase
export const sign_in_inputs = [
  {
    // textInputStyles,
    key: 'email',
    label: i18n.t('signIn.email'),
    errorInfoMessage: i18n.t('signIn.invalidEmail'),
    placeholder: i18n.t('signIn.emailPlaceHolder'),
    errorInfoMessageIcon: 'close-circle',
    errorInfoMessageIconColor: Colors.red,
    leftIcon: 'email-outline'
  },
  {
    key: 'password',
    label: i18n.t('signIn.password'),
    placeholder: i18n.t('signIn.passwordPlaceHolder'),
    errorInfoMessage: i18n.t('signIn.passwordInfo'),
    errorInfoMessageIcon: 'alert-circle',
    errorInfoMessageIconColor: Colors.blue,
    leftIcon: 'lock-outline',
    rightIcon: 'eye',
    secure: true
  }

]

// eslint-disable-next-line camelcase
export const sign_up_inputs = [
  {
    // textInputStyles,
    key: 'firstName',
    label: i18n.t('signUp.firstName'),
    placeholder: i18n.t(''),
  },
  {
    key: 'lastName',
    label: i18n.t('signUp.lastName'),
    placeholder: i18n.t(''),
  },
  {
    key: 'phone',
    label: i18n.t('signUp.phone'),
    rightIcon: 'check',
    placeholder: i18n.t(''),
  },
  {
    key: 'email',
    label: i18n.t('signUp.email'),
    placeholder: i18n.t(''),
    errorInfoMessage: i18n.t('signUp.emailAvailable'),
    errorInfoMessageIcon: 'check-circle',
    errorInfoMessageIconColor: Colors.check,
    leftIcon: 'email-outline',
  },
  {
    key: 'password',
    label: i18n.t('signUp.password'),
    placeholder: i18n.t(''),
    errorInfoMessage: i18n.t('signUp.passwordInfo'),
    errorInfoMessageIcon: 'alert-circle',
    errorInfoMessageIconColor: Colors.blue,
    leftIcon: 'lock-outline',
    rightIcon: 'eye',
    secure: true
  },
  {
    key: 'confirmPassword',
    label: i18n.t('signUp.confirmPassword'),
    placeholder: i18n.t(''),
    errorInfoMessage: i18n.t('signUp.passwordInfo'),
    errorInfoMessageIcon: 'alert-circle',
    errorInfoMessageIconColor: Colors.blue,
    leftIcon: 'lock-outline',
    rightIcon: 'eye',
    secure: true
  }

]
