import { StyleSheet, Dimensions, Platform } from 'react-native'
import {Metrics, Colors} from '../../Themes'

const subtractFactor = Platform.OS === 'android' ? 20 : 0

export default StyleSheet.create({
  backgroundImage: {
    flex: 1
  },
  body: {
    flex: 1,
    // alignItems: 'center',
    marginHorizontal: Metrics.doubleSection
  },
  faceRecognition: {
    backgroundColor: Colors.black
  },
  forgotPassword: {
    alignSelf: 'center',
    textAlign: 'center'
  },
  heading: {
    flexDirection: 'row',
    marginHorizontal: Metrics.doubleSection,
    justifyContent: 'space-between'
  },
  headingText: {
    // : 'flex-start'
  },
  main: {
    justifyContent: 'center',
    minHeight: Dimensions.get('window').height - subtractFactor
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.themeColor,
    marginHorizontal: Metrics.section
  },
  buttonText: {
    color: Colors.snow,
    padding: Metrics.baseMargin
  },
  signUpText: {
    // color: Colors.text
  }
})
