import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Fonts } from '../../Themes/'
import Colors from '../../Themes/Colors'
import { HP } from '../../Themes/Metrics'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    padding: Metrics.section,
    minHeight: HP(95)
  },
  titleText: {
    fontSize: Fonts.size.h3,
    fontFamily: Fonts.type.semiBold,
    marginTop: Metrics.section
  },
  descriptionText: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.interRegular,
    marginTop: Metrics.baseMargin
  },
  cardText: {
    flex: 1,
    color: Colors.grey,
    marginHorizontal: 30,
    fontSize: Fonts.size.small,
    fontFamily: Fonts.type.base
  },
  buttonCover: {
    flex: 1,
    marginTop: 30
  },
  contentCover: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  bottomView: {
    padding: 10
  },
  cardButton: {
    margin: 10,
    padding: 15,
    flexDirection: 'row',
    alignItems: 'center'
  },
  privacyText: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.interRegular,
    marginHorizontal: Metrics.doubleBaseMargin
  }
})
