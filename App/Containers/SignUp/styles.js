import { StyleSheet, Dimensions, Platform } from 'react-native'
import {Metrics, Colors} from '../../Themes'
import { sub } from 'react-native-reanimated'

const subtractFactor = Platform.OS === 'android' ? 37 : 0

export default StyleSheet.create({
  backgroundImage: {
    flex: 1
  },
  inputs: {
    flex: 1,
    marginHorizontal: Metrics.doubleSection
  },
  bottom: {
    // flex: 0.4,
    justifyContent: 'center'
  },
  faceRecognition: {
    backgroundColor: Colors.black
  },
  forgotPassword: {
    alignSelf: 'center',
    textAlign: 'center'
  },
  heading: {
    flex: 0.1,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: Metrics.section + Metrics.baseMargin,
    justifyContent: 'space-between'
  },
  headingText: {
    // : 'flex-start'
  },
  scrollView: {
    minHeight: Dimensions.get('window').height - subtractFactor
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.themeColor,
    marginVertical: Metrics.doubleBaseMargin,
    borderRadius: 10
  },
  buttonText: {
    color: Colors.snow,
    padding: Metrics.baseMargin
  },
  signUpText: {
    color: Colors.themeColor,
    padding: Metrics.baseMargin
  },
  termsContainer: {
    flexDirection: 'row',
  }
})
