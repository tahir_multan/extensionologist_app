import React, { Component } from 'react'
import { ScrollView, Text, SafeAreaView, View, TouchableOpacity } from 'react-native'
import { Images } from '../Themes'
import Icon from 'react-native-vector-icons/MaterialIcons'

// Styles
import styles from './Styles/LaunchScreenStyles'

const BUTTONS = [
  { icon: 'camera', text: 'Allow to access camera and photos' },
  { apps: 'camera', text: 'Allow to access apps' },
  { search: 'camera', text: 'Allow to access browser search' },
  { contacts: 'camera', text: 'Allow to access contacts' }
]
export default class LaunchScreen extends Component {

  renderButton = ({icon, text}) => {
    return (
      <TouchableOpacity key={text} style={styles.cardButton}>
        <Icon name={icon} size={24} />
        <Text>{text}</Text>
        <Icon name={icon} size={16} />
      </TouchableOpacity>
    )
  }
  render () {
    return (
      <SafeAreaView style={styles.mainContainer}>
        <ScrollView style={styles.container}>
          <Text style={styles.titleText}>Permissions</Text>
          <Text style={styles.descriptionText}>
            To use the application features you need to give this application all following permissions.
          </Text>
          {BUTTONS.map(this.renderButton)}
        </ScrollView>
      </SafeAreaView>
    )
  }
}
