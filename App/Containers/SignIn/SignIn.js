import React, { useState } from 'react'
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import { View, Text, ScrollView, ImageBackground, TouchableOpacity } from 'react-native'
import Input from '../../Components/Input'
import i18n from 'i18n-js'
// eslint-disable-next-line camelcase
import { sign_in_inputs } from '../../Constants/constant'
import {Images} from '../../Themes'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

const SignIn = () => {
  const [invalidEmail, setInvalidEmail] = useState(false)
  const [invalidPassword, setInvalidPassword] = useState(false)
  const [data, setData] = useState({})
  return (
    <KeyboardAwareScrollView contentContainerStyle={styles.scrollView}>
      <ScrollView contentContainerStyle={styles.scrollView}>
        <ImageBackground source={Images.backgroundImage} style={styles.backgroundImage}>
          <View style={styles.heading}>
            <Text style={styles.headingText}>{i18n.t('signIn.headingSignIn')}</Text>
            <Text style={styles.signUpText} onPress={() => Actions.signUp()}>{i18n.t('signIn.headingSignUp')}</Text>
          </View>
          <View style={styles.inputs}>
            {sign_in_inputs.map((input, index) => {
              const {key = ''} = input
              return <Input properties={{...input,
                onChangeText: (text) => setData({...data, [data[key]]: text})}}
                key={`${index}`}
              />
            })}
          </View>
          <View style={[styles.inputs, styles.bottom]}>
            <TouchableOpacity style={styles.button}>
              <Text style={styles.buttonText}>Sign In</Text>
            </TouchableOpacity>
            <Text style={styles.forgotPassword} onPress={() => Actions.forgotPassword()}>Forgot your Password?</Text>
            <TouchableOpacity style={[styles.button, styles.faceRecognition]}>
              <Text style={styles.buttonText}>Sign In with face recognition</Text>
            </TouchableOpacity>
            <Text style={styles.forgotPassword}>OR</Text>
          </View>
        </ImageBackground>
      </ScrollView>
    </KeyboardAwareScrollView>
  )
}

export default SignIn
