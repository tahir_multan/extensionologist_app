import React, { useState } from 'react'
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import { View, Text, ScrollView, ImageBackground, TouchableOpacity } from 'react-native'
import Input from '../../Components/Input'
import i18n from 'i18n-js'
// eslint-disable-next-line camelcase
import { sign_up_inputs } from '../../Constants/constant'
import {Images} from '../../Themes'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'

const SignIn = () => {
  const [invalidEmail, setInvalidEmail] = useState(false)
  const [invalidPassword, setInvalidPassword] = useState(false)
  const [data, setData] = useState({})
  return (
    <KeyboardAwareScrollView contentContainerStyle={styles.scrollView}>
      <ScrollView contentContainerStyle={styles.scrollView}>
        <ImageBackground source={Images.backgroundImage} style={styles.backgroundImage}>
          <View style={styles.heading}>
            <Text style={styles.headingText}>{i18n.t('signIn.headingSignUp')}</Text>
            <Text style={styles.signUpText} onPress={() => Actions.signUp()}>{i18n.t('signIn.headingSignIn')}</Text>
          </View>
          <View style={styles.inputs}>
            {sign_up_inputs.map((input, index) => {
              const {key = ''} = input
              return <Input properties={{...input,
                onChangeText: (text) => setData({...data, [data[key]]: text})}}
                key={`${index}`}
              />
            })}
          </View>
          <View style={[styles.inputs, styles.bottom]}>
            <TouchableOpacity style={styles.button}>
              <Text style={styles.buttonText}>{i18n.t('signUp.headingSignUp')}</Text>
            </TouchableOpacity>
            <View style={styles.termsContainer}>
              <Icon name={'check-circle'} />
              <Text style={styles.forgotPassword}>{i18n.t('signUp.terms')}</Text>
            </View>
          </View>
        </ImageBackground>
      </ScrollView>
    </KeyboardAwareScrollView>
  )
}

export default SignIn
